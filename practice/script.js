'use strict';

// Data needed for a later exercise
const flights =
  '_Delayed_Departure;fao93766109;txl2133758440;11:25+_Arrival;bru0943384722;fao93766109;11:45+_Delayed_Arrival;hel7439299980;fao93766109;12:05+_Departure;fao93766109;lis2323639855;12:30';

// Data needed for first part of the section
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],
  order: function (starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },
  orderPasta: function (ing1, ing2, ing3) {
    console.log(`here is your pasta with ${ing1},${ing2} and ${ing3}`);
  },
  orderPizza: function (mainIngredient, ...otherIngredient) {
    console.log(mainIngredient);
    console.log(otherIngredient);
  },
  orderDelivery: function ({
    starterIndex,
    mainIndex,
    time = '20:15',
    address,
  }) {
    console.log(`order received! ${this.starterMenu[starterIndex]}
     and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`);
  },

  openingHours: {
    thu: {
      open: 12,
      close: 22,
    },
    fri: {
      open: 11,
      close: 23,
    },
    sat: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },
};
restaurant.orderPizza('mushrooms', 'onion', 'olivers', 'spinach');
restaurant.orderDelivery({
  // time: '23:10',
  address: 'Via del sole, 21',
  mainIndex: 2,
  starterIndex: 2,
});
const { name, openingHours, categories } = restaurant;
console.log(name, openingHours, categories);

const {
  name: restaurantName,
  openingHours: hours,
  categories: tags,
} = restaurant;
console.log(restaurantName, hours, tags);
///default value
const { menu = [], starterMenu: starters = [] } = restaurant;
console.log(menu, starters);
//mutating variables
let a = 111;
let b = 999;
const obj = { a: 23, b: 7, c: 14 };
({ a, b } = obj);
console.log(a, b);
//nested objects

const {
  fri: { open, close },
} = openingHours;
console.log(open, close);
const {
  fri: { open: o, close: c },
} = openingHours;
console.log(o, c);

// const ingredients = [
//   prompt("let's make pasta! Ingredient 1?"),
//   prompt("let's make pasta! Ingredient 2?"),
//   prompt("let's make pasta! Ingredient 3?"),
// ];
// console.log(ingredients);
// restaurant.orderPasta(...ingredients);

//rest operator des
const arr2 = [1, 2, ...[3, 4]];
const [i, j, ...others] = [1, 2, 3, 4, 5];
console.log(i, j, others);

const [pizza, , risotto, ...otherFood] = [
  ...restaurant.mainMenu,
  ...restaurant.starterMenu,
];
console.log(pizza, risotto, otherFood);

const { sat, ...weekdays } = restaurant.openingHours;
console.log(weekdays);

//functions

const add = function (...numbers) {
  let sum = 0;
  for (let i = 0; i < numbers.length; i++) {
    sum += numbers[i];
  }
  console.log(sum);
};
add(2, 3);
add(5, 6, 3, 8);
add(8, 4, 7, 2, 0, 3, 5);

const x = [23, 5, 7];
add(...x);
// const arr = [2, 2, 4];

// const [x, y, z] = arr;
// console.log(x, y, z);
// console.log(arr);

// // const [first, second] = restaurant.categories;
// // console.log(first, second);
// let [main, , secondary] = restaurant.categories;
// console.log(main, secondary);
// // const temp = main;
// // main = secondary;
// // secondary = temp;
// // console.log(main, secondary);
// [main, secondary] = [secondary, main];
// console.log(main, secondary);

// // console.log(restaurant.order(2, 1));
// const [starter, mainCourse] = restaurant.order(2, 1);
// console.log(starter, mainCourse);

// const nested = [1, 3, [4, 5]];
// const [i, , j] = nested;
// const [p, , [q, r]] = nested;
// const [m, n, o] = [3, 4];
// console.log(i, j);
// console.log(p, q, r);
// console.log(m, n, o);

/*  spread operator   */

const arr = [3, 4, 5];
const badNewArr = [1, 2, arr[0], arr[1], arr[2]];
console.log(badNewArr);
const newArr = [6, 7, ...arr];
console.log(newArr);
console.log(...arr);
const newMenu = [...restaurant.mainMenu, 'Gnocci'];
console.log(newMenu);

const mainMenuCopy = [...restaurant.mainMenu];
const main = [...restaurant.mainMenu, ...restaurant.starterMenu];
console.log(main);

const str = 'jonas';
const letter = [...str, ' ', 'S.'];
console.log(letter);
// console.log(`${...str} s`); //cannot work in this case

/*----OR-----  */
console.log(3 || 'jonas');
console.log('' || 'jonas');
console.log(true || 0);
console.log(undefined || null);
console.log(undefined || 0 || '' || 'Hello' || 23 || null);
restaurant.numGuests = 23;
const guests1 = restaurant.numGuests ? restaurant.numGuests : 10;
console.log(guests1);
const guests2 = restaurant.numGuests || 10;
console.log(guests2);

/* -----AND-----*/
console.log(0 && 'jonas');
console.log(4 && 'jonas');
console.log('Hello' && 23 && null && 'jonas');
if (restaurant.orderPizza) restaurant.orderPizza('mushroom', 'spinach');

restaurant.orderPizza && restaurant.orderPizza('mushrooms', 'spinach');

/**---Nulish coalesing value:null and undefined [Not include 0 and ''] */

restaurant.numGuests1 = 0;
const guestsCorrect = restaurant.numGuests1 || 10;
console.log(guestsCorrect);

const guestsCorrect1 = restaurant.numGuests1 ?? 10;
console.log(guestsCorrect1);

const menu2 = [...restaurant.starterMenu, ...restaurant.mainMenu];

for (const item of menu2) console.log(item);

for (const [i, el] of menu2.entries()) {
  console.log(`${i + 1} : ${el}`);
}
